/*
* Json interface, Uses GET annotation based off post object to return information based off the objects stored variables.
 */

package com.example.rest_leckyahmed;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonApi {
    @GET("posts")
    Call<List<Post>> getPosts();
}