/*
* CIS2250 Rest Demonstration.
* Application created by following guide provided by Coding Flow.
* Application uses retrofit 2.4.0 to connect to "fake" api service and returns post related xml documentation.
* Source- https://codinginflow.com/tutorials/android/retrofit/part-1-simple-get-request.
* @ Jon Lecky and Tariq Ahmed
* since 30/01/2021
*
* Main Activity encapsulates three methods to dictate what actions are taken depending on response.
* OnCreate defines the base connection the api, we used an api service to get a return.
* OnResponse has a boolean failure flag that returns any error codes, if passed it will use the getters from the post object to return a string of the return.
 */
package com.example.rest_leckyahmed;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private TextView textViewResult;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewResult = findViewById(R.id.text_view_result);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonApi jsonBTW = retrofit.create(JsonApi.class);
        Call<List<Post>> call = jsonBTW.getPosts();
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if(!response.isSuccessful()){
                    textViewResult.setText("JL - FAILURE CODE =" + response.code());
                    return;
                }
                List<Post> posts = response.body();

                for (Post post: posts){
                    String content = "";
                    content += "ID: " + post.getId() + "\n";
                    content += "User ID: " + post.getUserId() + "\n";
                    content += "Title: " + post.getTitle() + "\n";
                    content += "Text: " + post.getText() + "\n\n";

                    textViewResult.append(content);
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }
        });
    }
}