package info.hccis.cis2250.canescamper.ui.camperList;

/**
 * This class will represent a camper.  It will be used for loading the data from the json string.
 * As well, it has the annotations needed to allow it to be used for the Room database.
 * @since 20200202
 * @author BJM
 */

public class Camper {



    private Integer id;
    private String firstName;
    private String lastName;
    private String dob;
    private Integer campType;
    private String campTypeDescription;



    public String getCampTypeDescription() {
        return campTypeDescription;
    }

    public void setCampTypeDescription(String campTypeDescription) {
        this.campTypeDescription = campTypeDescription;
    }



    public Camper() {
    }

    public Camper(Integer id) {
        this.id = id;
    }

    public Camper(Integer id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Integer getCampType() {
        return campType;
    }

    public void setCampType(Integer campType) {
        this.campType = campType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }



    @Override
    public String toString() {
        return "#"+id+" Name: "+this.firstName+" "+this.lastName;
    }

}
