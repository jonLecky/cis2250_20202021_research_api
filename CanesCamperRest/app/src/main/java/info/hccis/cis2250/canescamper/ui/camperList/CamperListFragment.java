package info.hccis.cis2250.canescamper.ui.camperList;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import info.hccis.cis2250.canescamper.R;
import info.hccis.cis2250.canescamper.ui.feedback.FeedbackViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CamperListFragment extends Fragment {

    private CamperListViewModel camperListViewModel;
    private TextView textViewResult;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        camperListViewModel =
                new ViewModelProvider(this).get(CamperListViewModel.class);
        View root = inflater.inflate(R.layout.fragment_camper_list, container, false);
        //final TextView textView = root.findViewById(R.id.text_feedback);
        camperListViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });

        textViewResult = root.findViewById(R.id.text_view_result);

        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://10.0.2.2:8081/api/CamperService/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    JsonCamperApi api = retrofit.create(JsonCamperApi.class);

    Call<List<Camper>> call = api.getCampers();


        call.enqueue(new Callback<List<Camper>>() {
        @Override
        public void onResponse(Call<List<Camper>> call, Response<List<Camper>> response) {
            if(!response.isSuccessful()){
                textViewResult.setText("Code" + response.code());
                return;
            }
            List<Camper> campers = response.body();

            for (Camper camper: campers){
                String content = "";
                content += "\n";
                content += "ID: " + camper.getId() + "\n";
                content += "First Name: " + camper.getFirstName() + "\n";
                content += "Last Name: " + camper.getLastName() + "\n";
                content += "DOB: " + camper.getDob() + "\n"+"\n";


                textViewResult.append(content);
            }
        }

        @Override
        public void onFailure(Call<List<Camper>> call, Throwable t) {
            textViewResult.setText(t.getMessage());
        }
    });


        return root;
    }
}