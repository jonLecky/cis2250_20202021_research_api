package info.hccis.cis2250.canescamper.ui.camperList;

import java.util.List;

import info.hccis.cis2250.canescamper.ui.camperList.Camper;
import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonCamperApi {

    /**
     * This abstract method to be created to allow retrofit to get list of campers
     * @return List of campers
     * @since 20200202
     * @author BJM (with help from the retrofit research.
     */

    @GET("campers")
    Call<List<Camper>> getCampers();

}
